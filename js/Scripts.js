<script>
    $(function(){
        //creamos una query que nos indicará que vamos a poner un atributo
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        
        // definimos el tiempo (en milisegundos) de rotación del carrusel
        $(".carousel").carousel({
            interval: 3000 
        });

        // nos muestra un mensaje cuando abrimos el modal, es decir nos muetra el mensaje cuando empieza a abrir el modal
        $('#enviar_consulta').on('show.bs.modal', function (e) {
            console.log('el Modal se está mostrando');
            // Cuando se abre el Modal, cambiamos el color del botón
            $('#contactoBtn_h1').removeClass('btn-outline-warning');
            $('#contactoBtn_h1').addClass('btn-secondary');
            $('#contactoBtn_h2').removeClass('btn-outline-warning');
            $('#contactoBtn_h2').addClass('btn-secondary');
            $('#contactoBtn_h3').removeClass('btn-outline-warning');
            $('#contactoBtn_h3').addClass('btn-secondary');
            $('#contactoBtn_h4').removeClass('btn-outline-warning');
            $('#contactoBtn_h4').addClass('btn-secondary');
            $('#contactoBtn_h5').removeClass('btn-outline-warning');
            $('#contactoBtn_h5').addClass('btn-secondary');
            $('#contactoBtn_h6').removeClass('btn-outline-warning');
            $('#contactoBtn_h6').addClass('btn-secondary');
            $('#contactoBtn_h7').removeClass('btn-outline-warning');
            $('#contactoBtn_h7').addClass('btn-secondary');

            //deshabilitamos el botón
            $('#contactoBtn_h1').prop('disabled', true);
            $('#contactoBtn_h2').prop('disabled', true);
            $('#contactoBtn_h3').prop('disabled', true);
            $('#contactoBtn_h4').prop('disabled', true);
            $('#contactoBtn_h5').prop('disabled', true);
            $('#contactoBtn_h6').prop('disabled', true);
            $('#contactoBtn_h7').prop('disabled', true);
        });
        // nos muestra un mensaje cuando acabemos de abrir el modal, es decir, nos manda el mensaje cuando ha terminado de abrir el modal
        $('#enviar_consulta').on('shown.bs.modal', function (e) {
            console.log('el Modal se mostró');
        });

        // nos muestra un mensaje cuando cerramos el modal, es decir nos muetra el mensaje cuando empieza a cerrar el modal
        $('#enviar_consulta').on('hide.bs.modal', function (e) {
            console.log('el Modal se está cerrando');
        });
        // nos muestra un mensaje cuando acabemos de cerrar el modal, es decir, nos manda el mensaje cuando ha terminado de cerrar el modal
        $('#enviar_consulta').on('hidden.bs.modal', function (e) {
            console.log('el Modal se cerró');
            // Cuando se cierra el Modal, cambiamos al color original del botón
            $('#contactoBtn_h1').removeClass('btn-secondary');
            $('#contactoBtn_h1').addClass('btn-outline-warning');
            $('#contactoBtn_h2').removeClass('btn-secondary');
            $('#contactoBtn_h2').addClass('btn-outline-warning'); 
            $('#contactoBtn_h3').removeClass('btn-secondary');
            $('#contactoBtn_h3').addClass('btn-outline-warning'); 
            $('#contactoBtn_h4').removeClass('btn-secondary');
            $('#contactoBtn_h4').addClass('btn-outline-warning'); 
            $('#contactoBtn_h5').removeClass('btn-secondary');
            $('#contactoBtn_h5').addClass('btn-outline-warning'); 
            $('#contactoBtn_h6').removeClass('btn-secondary');
            $('#contactoBtn_h6').addClass('btn-outline-warning'); 
            $('#contactoBtn_h7').removeClass('btn-secondary');
            $('#contactoBtn_h7').addClass('btn-outline-warning'); 
            //habilitamos el botón
            $('#contactoBtn_h1').prop('disabled', false);
            $('#contactoBtn_h2').prop('disabled', false);
            $('#contactoBtn_h3').prop('disabled', false);
            $('#contactoBtn_h4').prop('disabled', false);
            $('#contactoBtn_h5').prop('disabled', false);
            $('#contactoBtn_h6').prop('disabled', false);
            $('#contactoBtn_h7').prop('disabled', false);
        });
    });
</script>